async function preInit(inputObj) {

}

async function postInit(inputObj) {
    console.log(`\n     _____            _            ______             _   
    /  ___|          (_)           | ___ \\           | |  
    \\ \`--. _ __  _ __ _ _ __   __ _| |_/ / ___   ___ | |_ 
     \`--. \\ '_ \\| '__| | '_ \\ / _\` | ___ \\/ _ \\ / _ \\| __|
    /\\__/ / |_) | |  | | | | | (_| | |_/ / (_) | (_) | |_ 
    \\____/| .__/|_|  |_|_| |_|\\__, \\____/ \\___/ \\___/ \\__|
          | |                  __/ |                      
          |_|                 |___/                       
                                        `)
    console.log(`\n    Welcome to the start-springboot application
     This application requires to open these services: 
         FC : https://fc.console.aliyun.com/
         ACR: https://cr.console.aliyun.com/
     This application can help you quickly deploy the SpringBoot project:
         Full yaml configuration    : https://github.com/devsapp/sprintboot#%E5%AE%8C%E6%95%B4yaml
         SpringBoot development docs: https://spring.io/projects/spring-boot/
     This application homepage: https://github.com/devsapp/start-springboot\n`)
}

module.exports = {
    postInit,
    preInit
}
