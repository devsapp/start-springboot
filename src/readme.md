# 阿里云函数计算：SpringBoot 应用

## 准备

在部署 SpringBoot 应用之前，需要您准备一个阿里云的账号，若该账号是一个子账号，则该账号需要有如下权限：

- AliyunVPCFullAccess
- AliyunNASFullAccess
- PassRole

上述 ```PassRole``` 为自定义权限策略，其内容为:

```json
 {
      "Statement": [
          {
              "Action": [
                  "ram:PassRole"
              ],
              "Effect": "Allow",
              "Resource": "*"
          }
      ],
      "Version": "1"
  }
```

如果您需要为函数配置 SLS 日志服务，并且此项配置为 ```Auto```, 则会自动给您的函数创建对应的 logproject 以及 logstore，因此还需要该账号具有如下权限：

- AliyunLogFullAccess

如果您未指定 ```role```，那么会自动给您的函数创建相应的角色并绑定如下的策略：

- AliyunECSNetworkInterfaceManagementAccess
- AliyunContainerRegistryReadOnlyAccess
- AliyunLogFullAccess（配置了 ```Auto``` 日志服务时才有）

如果您指定了 ```role``` 并且指定了相关的策略,则会将这些策略 attach 到您的 ```role``` 上。

此时需要该账号具备如下权限：

- AliyunRAMFullAccess

## 开发

若您只想运行我们的示例代码，可以跳过此阶段。

您可以在 ```./code``` 目录下进行开发，或者将您已有的 springboot 项目迁移到 ```./code``` 目录下。

开发完成后，在 ```./code``` 下运行 ```./mvnw package``` 指令进行打包，该指令可以直接放在 ```s.yaml``` 中的 ```services.${name}.actions.pre-deploy``` 字段下，表示在部署前触发打包指令。

打包完成后，您要确保 ```s.yaml``` 中的 ```srcCode``` 字段为您打包生成的 jar 包路径。

若您想指定应用的监听端口，则在 ```s.yaml``` 中，对 ```services.${name}.props.function.customContainerConfig.command``` 字段下的 ```-Dserver.port=9000``` 部分进行更新即可，默认监听 9000 端口。

## 部署

执行 ```s deploy``` 即可进行部署。

## 运行

部署完成后，可以访问返回的 url 访问函数，此时您可以执行：

- s logs : 查看日志
- s metrics : 查看函数监控指标

## 删除

执行 ```s remove``` 即可移除所有资源。